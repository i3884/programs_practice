import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result34{

	/*
	 * Complete the 'migratoryBirds' function below.
	 *
	 * The function is expected to return an INTEGER. The function accepts
	 * INTEGER_ARRAY arr as parameter.
	 */

	public static void migratoryBirds(List<Integer> arr) {

		Map<Integer, Integer> m = new HashMap<>();
		for (int i = 0; i < arr.size(); i++) {
			if (!m.keySet().isEmpty()) {
				if (m.keySet().contains(arr.get(i))) {
					int count = m.get(arr.get(i));
                    m.put(arr.get(i), count+1);
				}
				else {
					m.put(arr.get(i), 1);
				}
			} else {
				m.put(arr.get(i), 1);
			}
		}
		//Collections.sort(list);

	}

}

public class HRMigratoryBirds {
	public static void main(String[] args) throws IOException {
	// Result.migratoryBirds(arr);

	}
}
