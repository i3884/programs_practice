import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result1 {

    /*
     * Complete the 'camelcase' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING s as parameter.
     */

    public static int camelcase(String s) {
   int count =0;
   for(int i=0;i<s.length();i++) {
	   if(s.charAt(i)>=65 && s.charAt(i)<=90) {
		   count++;
	   }
   }
   if(s.length()!=0) {
   return count+1;
   }
   else {
	   return 0;
   }
}

}
public class HrCamelCase {
    public static void main(String[] args) throws IOException {
       // BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
       // BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

       // String s = bufferedReader.readLine();
    	String s="thisIsABeautifulAIsWorld";

        int result = Result1.camelcase(s);
        System.out.println(result); 
        

        //bufferedWriter.write(String.valueOf(result));
        //bufferedWriter.newLine();

        //bufferedReader.close();
       // bufferedWriter.close();
    }
}