package basicproblemsolving;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NumbersBeginWith {

	public static void main(String[] args) {
		
	/*	String sa="book";
		var h=sa.chars()
		.mapToObj(c->(char)c)
		.collect(Collectors.groupingBy(Character::new,Collectors.counting()))
		.entrySet()
		.stream()
		.filter((e)->e.getKey()==1)
		.peek(System.out::println)
		.collect(Collectors.toList()
			);
		System.out.println(h);
		 
		
		
		var result=List.of(1,32,13,13,56)
		.stream()
		.map(s->s+" ")
		.filter(s->s.startsWith("1"))
		.collect(Collectors.groupingBy(String::new,Collectors.counting()));
		System.out.println(result); 
		Set<Integer> set=new HashSet<>();
	
		
	Stream.of(1,2,3,4,3,4).filter(n->set.add(n))
	.collect(Collectors.toList())
		.forEach(System.out::println);*/
	
	var r=Stream.of(1,2,3)
			.max(Comparator.comparing(Integer::valueOf));
	System.out.println(r);
        
		
	
	var sum=List.of(1,32,13,13,56).stream().collect(Collectors.summarizingInt(Integer::new));
	System.out.println(sum);
		//List.of(1,32,13,13,56).stream()
	String sa="book";
	var re=sa.chars()
			.mapToObj(c->(char)c)
			.collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
			.entrySet()
			.stream()
			.filter(e->e.getValue()>1L)
			.map(e->e.getKey())
			.findFirst();
			
			System.out.println(re.get()); 
			Stream.of(1,2,3,4,3,4)
			.sorted(Collections.reverseOrder());

		

	}

}
