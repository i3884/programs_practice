package basicproblemsolving;

import java.util.Arrays;
import java.util.List;

class Resulxcvt {

	/*
	 * Complete the 'divisibleSumPairs' function below.
	 *
	 * The function is expected to return an INTEGER. The function accepts following
	 * parameters: 1. INTEGER n 2. INTEGER k 3. INTEGER_ARRAY ar
	 */

	public static int divisibleSumPairs(int n, int k, List<Integer> ar) {
		int count=0;
   for(int i=0;i<n;i++) {
	   for(int j=0;j<n;j++) {
		   if(!(i==j)) {
			   if((ar.get(i)+ar.get(j))%k==0) {
				   count++;
			   }
		   }
	   }
   }
		
		return count/2;

	}

}

public class HRDivisibleNumberPairs {
	public static void main(String[] args) {

		System.out.println(Resulxcvt.divisibleSumPairs(6, 3, Arrays.asList(1, 3, 2, 6, 1, 2)));

	}
}
