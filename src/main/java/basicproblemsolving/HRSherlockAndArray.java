package basicproblemsolving;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class sdfsdf {

    /*
     * Complete the 'balancedSums' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts INTEGER_ARRAY arr as parameter.
     */

    public static String balancedSums(List<Integer> arr) {

    	Integer total=arr.stream().mapToInt(Integer::valueOf).sum();
    	boolean flag=false;
    	int sum1=0;
    	for( int b=0;b<arr.size();b++) {
    	    		
    		if(total-arr.get(b)-sum1==sum1) {
    			flag=true;
    			break;
    		}
    		else {
    			sum1+=arr.get(b);
    		}
    		  		
    	}
if(flag) {
	return "YES";
}
else {
    	
    	return "NO";}

    }

}

public class HRSherlockAndArray {
    public static void main(String[] args)  {
 System.out.println( sdfsdf.balancedSums(Stream.of(5,6,8,11).collect(toList())));

              
    }
}
