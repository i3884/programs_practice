package basicproblemsolving;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

class dfs {
	public static List<Integer> missingNumbers(List<Integer> arr, List<Integer> brr) {
		Set<Integer> s = new TreeSet<>();
		
		Map<Integer,Integer> m1=new HashMap<>();
		Map<Integer,Integer> m2=new HashMap<>();
		for(int i=0;i<arr.size();i++) {
			Integer temp=arr.get(i);
			if(m1.get(temp)==null) {
				m1.put(temp,1);
			}
			else {
				Integer count=m1.get(temp);
				m1.put(temp, count+1);
			}
		}
		
		for(int i=0;i<brr.size();i++) {
			Integer temp=brr.get(i);
			if(m2.get(temp)==null) {
				m2.put(temp,1);
			}
			else {
				Integer count=m2.get(temp);
				m2.put(temp, count+1);
			}
		}
		
		for(Map.Entry<Integer, Integer> e:m2.entrySet()) {
			if(!m1.keySet().contains(e.getKey())) {
				s.add(e.getKey());
			}
			else {
				if(!m1.get(e.getKey()).equals(e.getValue())) {
					s.add(e.getKey());
				}
			}
		}
		
		

		return s.stream().collect(toList());

	}

}

public class HRMissingNumbers {
	public static void main(String[] args)  {

		System.out.println(
				dfs.missingNumbers(Stream.of(203, 204, 205, 206, 207, 208, 203, 204, 205, 206).collect(toList()),
						Stream.of(203, 204, 204, 205, 206, 207, 205, 208, 203, 206, 205, 206, 204).collect(toList())));

	}
}
