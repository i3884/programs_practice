import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result17 {

    /*
     * Complete the 'beautifulBinaryString' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING b as parameter.
     */

    public static int beautifulBinaryString(String b) {
  
    	Pattern p=Pattern.compile("010");
    	Matcher m=p.matcher(b);
    	int count=0;
    	while(m.find()) {
    		count++;
    	}
    	
return count;
    }

}

public class HRbeautifulString {
    public static void main(String[] args) throws IOException {
        

  System.out.println(Result17.beautifulBinaryString("0100101010"))	;

      
    }
}
