package com.praveena.strings;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class Resultsdf {

    /*
     * Complete the 'stringConstruction' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING s as parameter.
     */

    public static int stringConstruction(String s) {
    	Map<Character,Integer> m=new HashMap<>();
    	for(int i=0;i<s.length();i++) {
    		char ch=s.charAt(i);
    		if(m.get(ch)==null) {
    			m.put(ch, 1);    			
    		}
    	}
    	int count=0;
    	for(Integer i:m.values()) {
    		count+=i;
    	}
    	
    	
    	
    	
		return count;
   

    }

}

public class HRStringConstruction {
    public static void main(String[] args) throws IOException {
      

                int result = Resultsdf.stringConstruction("abcabc");

              System.out.println(result); 
              
    }
}
