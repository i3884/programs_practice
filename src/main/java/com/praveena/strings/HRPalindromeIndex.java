package com.praveena.strings;

import java.io.IOException;

class Resultsfd {

	/*
	 * Complete the 'palindromeIndex' function below.
	 *
	 * The function is expected to return an INTEGER. The function accepts STRING s
	 * as parameter.
	 */

	public static int palindromeIndex(String s) {
		if(s.length()!=0) {		
			if (s.equals(new StringBuilder(s).reverse().toString())) {
				return -1;
			} else {
				for (int i = 0; i < s.length() ; i++) {
					String s1=new StringBuilder(s).deleteCharAt(i).toString();
					String s2=new StringBuilder(s1).reverse().toString();
					System.out.println("s1==>"+s1+"s2==>"+s2);
					if(s1.equals(s2)) {
											return i;
					}
					
				}
				}
		}
			return -1;

	

}
}

public class HRPalindromeIndex {
	public static void main(String[] args) throws IOException {

		int result = Resultsfd.palindromeIndex("aaab");
		System.out.println(result);

	}
}
