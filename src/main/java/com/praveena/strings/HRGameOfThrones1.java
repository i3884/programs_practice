package com.praveena.strings;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class Resultsfs{

    /*
     * Complete the 'gameOfThrones' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */

    public static String gameOfThrones(String s) {
    	
    	Map<Character,Integer> m= new HashMap<>();
    	
    	for(int i=0;i<s.length();i++) {
    		Character ch=s.charAt(i);
    		if(m.get(ch)==null) {
    			m.put(ch, 1);
    		}
    		else {
    			int cur=m.get(ch);
    			m.put(ch, cur+1);
    		}
    	}
    	
    	
    	int count=0;
    	for(Integer i:m.values()) {
    		if(count>1) {
    			break;
    		}
    		if(i%2!=0) {
    			count=count+1;    			
    		}
    		
    	}
    
    	if(count>1) {
    		return "NO";
    	}
    	else {
    		return "YES";
    	}
    	
	
    
    
    }

}

public class HRGameOfThrones1 {
    public static void main(String[] args) throws IOException {
      

       System.out.println( Resultsfs.gameOfThrones("cdefghmnopqrstuvw"));

    }
}
