package com.praveena.strings;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Resultdf {

    /*
     * Complete the 'makingAnagrams' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. STRING s1
     *  2. STRING s2
     */

    public static int makingAnagrams(String s1, String s2) {
   
    	int toDelete=0;
    	Map<Character,Integer> m=new HashMap<Character, Integer>();
    	
    	
    	for(int i=0;i<s1.length();i++) {
    		if(m.get(s1.charAt(i))==null) {
    			m.put(s1.charAt(i), 1);
    		    		}
    		else {
    			int cur=m.get(s1.charAt(i));
    			m.put(s1.charAt(i), cur+1);
    		}
    	}
    	
    	for(int i=0;i<s2.length();i++) {
    		
    		if(m.keySet().contains(s2.charAt(i))) {
    		if(m.get(s2.charAt(i))==1) {
    			m.remove(s2.charAt(i));
    		    		}
    		else {
    			int cur=m.get(s2.charAt(i));
    			m.put(s2.charAt(i), cur-1);
    		}
    		}
    		else {
    			toDelete++;
    		}
    	}
    	
    	
    	
    	 for(Integer i: m.values()){
    		 toDelete=toDelete+i;
         }
    	
    	
    	
    	return toDelete;
    }

}

public class HRMakingAnagrams {
    public static void main(String[] args) throws IOException {
    	System.out.println(Resultdf.makingAnagrams("abcde", "dcab"));

    }
}
