package com.praveena.strings;

import java.util.HashMap;
import java.util.Map;

class rert {

	public static String twoStrings(String s1, String s2) {
		Map<Character, Integer> m = new HashMap<>();
		boolean flag = false;
		for (int i = 0; i < s1.length(); i++) {
			char ch = s1.charAt(i);
			if (m.get(ch) == null) {
				m.put(ch, 1);
			}
		}

		for (int i = 0; i < s2.length(); i++) {
			char ch = s2.charAt(i);
			if (m.get(ch) != null) {
				flag = true;
				break;
			}
		}

		if (flag) {
			return "YES";
		} else {
			return "NO";
		}

	}

}

public class HRTwoStrings {
	public static void main(String[] args) {

		String result = rert.twoStrings("asfdbc", "jkljk");
		System.out.println(result); 
		

	}
}
