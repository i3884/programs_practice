package com.praveena.strings;

import java.util.HashMap;
import java.util.Map;

class Resultksd {

	/*
	 * Complete the 'anagram' function below.
	 *
	 * The function is expected to return an INTEGER. The function accepts STRING s
	 * as parameter.
	 */

	public static int anagram(String s) {
		Map<Character, Integer> m = new HashMap<>();
		Map<Character, Integer> m2 = new HashMap<>();
		String s1 = s.substring(0, s.length() / 2);
		String s2 = s.substring(s.length() / 2);

		if (s1.length() == s2.length()) {
			for (int i = 0; i < s1.length(); i++) {
				char ch = s1.charAt(i);
				if (m.get(ch) == null) {
					m.put(ch, 1);
				} else {
					int cur = m.get(ch);
					m.put(ch, cur + 1);
				}
			}
			for (int i = 0; i < s2.length(); i++) {
				char ch = s2.charAt(i);
				if (m.containsKey(ch)) {
					if (m.get(ch) == 1) {
						m.remove(ch);
					} else {
						int cur = m.get(ch);
						m.put(ch, cur - 1);
					}
				} else {
					m2.put(ch, 1);
				}
			}
			System.out.println(m);
			System.out.println(m2);
			int count=0;
			int count2=0;
			for(Integer i:m.values()) {
				count+=i;
			}
			for(Integer i:m.values()) {
				count2+=i;
			}
			if(count>=count2) {
				return count;
				}
			else {
				return count2;
			}
			

		} else {
			return -1;
		}

	}

}

public class HRAnagram {
	public static void main(String[] args) {
		System.out.println(Resultksd.anagram("aaabbb"));

	}
}