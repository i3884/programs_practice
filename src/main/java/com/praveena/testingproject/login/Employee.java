package com.praveena.testingproject.login;

public class Employee {

	
	private String name;
	private int salary;
	private Department department;
	
	public Employee(String name, int salary) {
		super();
		this.name = name;
		this.salary = salary;
	}
	
	public Employee(String name, int salary,Department department) {
		super();
		this.name = name;
		this.salary = salary;
		this.department=department;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	
}
