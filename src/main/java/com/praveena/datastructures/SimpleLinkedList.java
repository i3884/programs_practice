package com.praveena.datastructures;

import java.util.NoSuchElementException;

public class SimpleLinkedList {

	private Node first;
	private Node last;
	private int size;

	private class Node {

		public int value;
		public Node next;

		Node(int value) {
			this.value = value;

		}
	}

	public void addFirst(int item) {
		Node node = new Node(item);
		if (isEmpty()) {
			first = last = node;
		} else {
			Node second = first;
			first = node;
			first.next = second;
		}
size++;
	}

	public void addLast(int item) {
		Node node = new Node(item);
		if (isEmpty()) {
			first = last = node;
		} else {
			last.next = node;
			last=node;
			last.next = null;
		}
		size++;
	}

	public void removeFirst() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		if (first == last) {
			first = last = null;
					}
		else {
		Node second = first.next;
		first.next = null;
		first = second;}
		size--;
	}

	public void removeLast() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		if (first == last) {
			first = last = null;
			
		}
		else {
		Node previous=previousNode(last);
		last=previous;
		last.next=null;}
		size--;

	}

	public Boolean isEmpty() {
		return first == null;
	}

	public Node previousNode(Node node) {
		Node current=first;
		while (current != null) {
			if (current.next == node) {
				return current;
			}
			current = current.next;
		}
		return null;
	}

	public int indexOf(int item) {
		int index = 0;
		Node current = first;
		while (current != null) {
			if (current.value == item) {
				return index;
			}
		}
		return -1;
	}

	public Boolean contains(int item) {
		return indexOf(item) != -1;
	}

	public void print() {
		Node current = first;
		while (current != null) {
			System.out.println(current.value);
			current = current.next;
		}
	}
	
	public int size() {
		return size;
	}
}
