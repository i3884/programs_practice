package com.praveena.datastructures;

public class TestSimpleLinkedList {

	public static void main(String[] args) {
		SimpleLinkedList list = new SimpleLinkedList();
		list.addFirst(10);
		list.addFirst(20);
		list.addLast(30);
		list.removeFirst();
		list.removeLast();
		list.print();
		System.out.println(list.size());
	}

}
