package com.praveena.programs;

import java.util.Arrays;
import java.util.List;

public class QuickSort {

	public static void main(String[] args) {


		Integer[] arr=new Integer[] {1,4,12,42,5,1,5};
		quickSort(arr, 0, arr.length-1);
		
		Arrays.stream(arr).forEach(System.out::println);

	}
	
	public static void quickSort(Integer[] arr,int left,int right) {
		int index=partition(arr,left,right);

		if(left<index-1) {
			quickSort(arr, left, index-1);
			
		}
		if(index<right) {
			quickSort(arr, index, right);
		}

	}
	
	public static int partition(Integer[] arr,int left,int right) {
		int i=left;int j=right;
		int pivot=arr[(i+j)/2];
		
		while(i<=j) {
			while(arr[i]<pivot) {
				i++;
			}
			while(arr[j]>pivot) {
				j--;
			}
			if(i<=j) {
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
				i++;
				j--;
			}
			
		}
		return i;
	}

}
