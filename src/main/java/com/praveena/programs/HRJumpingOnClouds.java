package com.praveena.programs;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Resuxcvlt {

    /*
     * Complete the 'jumpingOnClouds' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts INTEGER_ARRAY c as parameter.
     */

    public static int jumpingOnClouds(List<Integer> c) {
 
    	int single=0;
    	int doubles=0;
    	for(int i=0;i<c.size();i++) {
    		if(!(c.get(i)==1)) {
    			single++;    			
    		}    		
    	}
    	for(int i=0;i<c.size();i=i+2) {
    		if(!(c.get(i)==1)) {
    			doubles++;    			
    		}    		
    	}
    	
    	if(single<doubles) {
    		return single;
    	}
    	else {
    		return doubles;
    	}
    	
    	

    }

}

public class HRJumpingOnClouds {
    public static void main(String[] args) throws IOException {
     

        int result = Resuxcvlt.jumpingOnClouds(Stream.of(0,0,1,0,0,1,1).collect(toList()));
        System.out.println(result); 
        

       
    }
}
