package com.praveena.programs;

import java.util.HashMap;
import java.util.Map;

public class CountOfLetters {

	public static void main(String[] args) {
		String a="Interview";
		char[] arr=a.toCharArray();
		Map<Character,Integer> mapArr=new HashMap<>();
		for(char ch:arr) {
			if(mapArr.keySet().contains(ch)) {
		mapArr.put(ch, mapArr.get(ch)+1);
		}
			else {
				mapArr.put(ch, 1);
			}
				
			}
		System.out.println(mapArr);
	}

}
