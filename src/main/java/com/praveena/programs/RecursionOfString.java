package com.praveena.programs;

public class RecursionOfString {

	public static void main(String[] args) {

String s="Praveena";

System.out.println(reverseString(s));

	}

	public static String reverseString(String str) {
		
		if ((str==null)||(str.length() <= 1))
	         return(str);
	        else
	        {
	           String reverse=reverseString(str.substring(1))+str.charAt(0);
	           return reverse;
	        }
		
	}
}
