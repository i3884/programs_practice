package com.praveena.programs;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Resulszdft {

	/*
	 * Complete the 'lonelyinteger' function below.
	 *
	 * The function is expected to return an INTEGER. The function accepts
	 * INTEGER_ARRAY a as parameter.
	 */

	public static int lonelyinteger(List<Integer> a) {

		int result = 0;

		for (Integer i : a) {
			if (a.indexOf(i) == a.lastIndexOf(i)) {
				result = i;
				break;

			}

		}
		return result;
	}

}

public class HRLonelyInteger {
	public static void main(String[] args) {
		System.out.println(Resulszdft.lonelyinteger(new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 1, 2, 3))));

	}
}
