package com.praveena.programs;

public class BinarySearch {

	public static void main(String[] args) {

		int[] arr= {10,20,55,66,88};
		int result=binarySearch(88, arr);
		if(result<0) {
			System.out.println("Not found");
		}
		

	}
	
	public static int binarySearch(int searchTerm,int[] arr) {
		
		int lowIndex = 0;
		int highIndex =arr.length;
		 		while(lowIndex<highIndex) {
			int middle=(highIndex+lowIndex)/2;
			if(searchTerm<arr[middle]) {
			highIndex=middle-1;
			    		}
		else if(searchTerm>arr[middle]) {			
			lowIndex=middle+1;			
		}
		else {
			System.out.println("found at index"+middle);
			return middle;
			//or set lowindex=highIndex+1;
		}					
		
		}
		
		return -1;
	}

}
