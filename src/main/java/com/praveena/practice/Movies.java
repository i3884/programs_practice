package com.praveena.practice;

public class Movies implements Comparable<Movies>{

	private String name;
	private int likes;
	private Genre genre; 
	
	

	public Movies(String name, int likes) {
		super();
		this.name = name;
		this.likes = likes;
	}
	
	public Movies(String name, int likes,Genre genre) {
		super();
		this.name = name;
		this.likes = likes;
		this.genre = genre;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}
	
	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}


@Override
public int compareTo(Movies o) {
	String s1 = this.getName();
	Movies m=(Movies)o;
	String s2=m.getName();
	return s1.compareTo(s2);
}

}

