package com.praveena.practice;

import java.util.function.Consumer;
import java.util.function.Function;

public class TestFunction {

	public static void main(String[] args) {
		
		
		Function<String,String> replaceColon=str->str.replace(":", "=");
		
		Function<String,String> addBraces=str->"{"+str+"}";
		
		System.out.println(replaceColon.andThen(addBraces).apply("Key:Value"));
		
		String s=replaceColon.andThen(addBraces).apply("Key:Value");
		
		Consumer<String> c=System.out::println;
		c.accept(s);
		
	
	}

}
