package com.praveena.practice;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class Reducers {

	public static void main(String[] args) {
		//count
		
	   List.of(1,2,3,5,2,3,4).stream().count();
	   
	   //anyMatch
	 System.out.println("contains even "+ 
	 List.of(1,2,3,5,2,3,4)
	   .stream()
	    .anyMatch(n -> n%2==0));
	 
	 System.out.println("All even "+ 
			 List.of(1,2,3,5,2,3,4)
			   .stream()
			    .allMatch(n -> n%2==0));
	 
	 
	 System.out.println("All even "+ 
			 List.of(1,2,3,5,2,3,4)
			   .stream()
			    .allMatch(n -> n%2!=0));
	 
	 
	 System.out.println("Find First"+ 
			 List.of(1,2,3,5,2,3,4)
			   .stream()
			    .findFirst().get());
	 
	 Integer i=Set.of(1,0,3,4)
	 .stream()
	 .findFirst()
	 .get();

	 System.out.println("From set "+i); 
	 
	 System.out.println(List.of("a","b","bsd","hsdfs","sdfsf")
	 .stream()
	 .max(Comparator.comparing(String::length))
	 .get()
	 );
	 
	 
	 
	}

}
