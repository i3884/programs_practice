package com.praveena.practice;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;
import java.util.stream.*;

public class MappingStreams {

	public static void main(String[] args) {
		
		List<Movies> list=Stream.of(new Movies("RRR", 10),new Movies("gg",23)).collect(toList());
		
		list.stream().map(movie->movie.getName()).forEach(System.out::println);
		
		//flatMap
		
		Stream.of(Arrays.asList(new Integer[]{1,2,3}),Arrays.asList(new Integer[]{1,2,3})).flatMap(l->l.stream()).forEach(System.out::println);
		
		
		

	}

}
