package com.praveena.practice;


import java.util.stream.Stream;

public class SlicingStreams {

	public static void main(String[] args) {

		
		Stream.iterate(1,n->n+1)
		.filter(n->n%2==0)
		.limit(10)
		.skip(5)
		.forEach(System.out::println);
		
   Stream.of(new Movies("a",10),new Movies("b",20),new Movies("c",40))
   .takeWhile(movie->movie.getLikes()<40)
   .forEach(movie->System.out.println(movie.getName()));
   
   Stream.of(new Movies("a",10),new Movies("b",20),new Movies("c",40))
   .dropWhile(movie->movie.getLikes()<40)
   .forEach(movie->System.out.println(movie.getName()));
  	

	}

}
