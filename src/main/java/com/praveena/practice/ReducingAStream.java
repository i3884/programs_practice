package com.praveena.practice;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.praveena.datastructures.Array;

public class ReducingAStream {

	public static void main(String[] args) {
		
		Integer sum=List.of(
		new Movies("A",10),
		new Movies("B",20),
		new Movies("C",20)
		)
		.stream()
		.map(Movies::getLikes)
		.reduce(Integer::sum)
		.get();
		System.out.println(sum);
		
		
		Integer i=new ArrayList<Integer>()
				  .stream()
				  .reduce(0, Integer::sum);
		System.out.println(i); 
		
		Optional<Integer> i1=new ArrayList<Integer>()
				             .stream()
				             .reduce(Integer::sum)
				             ;
		System.out.println(i1.orElse(0));
		
				                
		

	}

}
