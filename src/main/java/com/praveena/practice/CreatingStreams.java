package com.praveena.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class CreatingStreams {

	public static void main(String[] args) {
		List<Integer> l= new ArrayList<>();
		
		l.stream().forEach(System.out::print);

		// Arrays
		
		Arrays.stream(new Integer[] {1,2,3}).forEach(System.out::println);
		
		//Stream
		
		Stream.of("vaishu","Vijay","bhagya").forEach((str)->System.out.println(str));
		
		//Indefinite streams
		
		Stream.generate(Math::random).limit(3).forEach(System.out::println);
		Stream.iterate(1, n->n+1).limit(10).forEach(n->System.out.println(n*13));
	}

}
