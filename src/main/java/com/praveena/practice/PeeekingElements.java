package com.praveena.practice;

import java.util.stream.Stream;

public class PeeekingElements {

	public static void main(String[] args) {
		Stream.of("Ga","bgd","jdak")
        .filter(s->s.contains("a"))
        .peek(s->System.out.println("fiktered "+s))
        .forEach(System.out::println);

	}

}
