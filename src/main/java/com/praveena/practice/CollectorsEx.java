package com.praveena.practice;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectorsEx {

	public static void main(String[] args) {
		
	List.of("sdf","sdfsfd","sdfsdf")
		.stream()
		.collect(Collectors.toList());
	
	
System.out.println(List.of("sdf","sdfsfd","sdfsdf")
	.stream()
	.collect(Collectors.toMap(Function.identity(), String::length)))
	 ;

var result=List.of(1,4,24,134,52)
           .stream()
           .filter(n->n>3)
           .collect(Collectors.summarizingInt(Integer::valueOf));
System.out.println(result);

var result1=List.of("sdf","sdfsfd","sdfsdf")
.stream()
.collect(Collectors.summarizingInt(String::length));
        
System.out.println(result1); 


var result3=List.of(
		new Movies("A",10),
		new Movies("B",20),
		new Movies("C",20)
		)
        .stream()
        .map(Movies::getName)
        .collect(Collectors.joining("_"));

System.out.println(result3);
	}
	
	

}
