package com.praveena.practice;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortedStreams {

	public static void main(String[] args) {
	//old way
		/*Stream.of(new Movies("a",10),new Movies("z",20),new Movies("c",40))
		.sorted().forEach(Movies->System.out.println(Movies.getName()));*/
		Stream.of(new Movies("a",10),new Movies("z",80),new Movies("c",40))
		.sorted(Comparator.comparing(Movies::getLikes).reversed())
		.forEach(movies->System.out.println(movies.getName()));

		List.of("a","b","bsd","hsdfs","sdfsf")
		 .stream()
		 .sorted(Comparator.comparing(String::length))
		 .forEach(System.out::println);
		List<String> l=Stream.of("asfgsfdgsfg","b","bsd","hsdfs","sdfsf").collect(Collectors.toList());
	l.sort(Comparator.comparing(String::length).reversed());
	System.out.println(l);
		
	}

}
