package com.praveena.practice;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class TestConsumer {

	public static void main(String[] args) {
	List<Integer> l=Stream.of(1,2,3).collect(toList());
	
		l.forEach(System.out::println);
		List<String> s=Stream.of("a","b","c").collect(toList());
		Consumer<String> print=System.out::println;
		Consumer<String> printUpperCase=item->System.out.println(item.toUpperCase());
		
		s.forEach(print.andThen(printUpperCase).andThen(print));
	}

}
