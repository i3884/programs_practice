package com.praveena.practice;

import java.util.stream.Stream;

public class Filtering {

	public static void main(String[] args) {

		Stream.of(new Movies("a", 10), new Movies("b", 20))
		.filter(movie -> movie.getLikes() > 10)
		.map(movie -> movie.getLikes())
		.forEach(System.out::println);
		
		
	}

}
