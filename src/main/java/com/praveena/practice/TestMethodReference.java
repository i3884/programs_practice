package com.praveena.practice;

public class TestMethodReference {

	private String s;
	public void m1(String s) {
		System.out.println(s); 
		
	}
	public static String m2(String s) {
		System.out.println(s);
		return s;
	}
	public TestMethodReference() {
		
	}
	public TestMethodReference(String s) {
		System.out.println(s); 
	}
	public static void main(String[] args) {
	
		TestMethodReference t=new TestMethodReference();
		
		Interf i1=TestMethodReference::m2;
		i1.print("static method");
		i1=t::m1;
		i1.print("instance method");
		i1=TestMethodReference::new;
		i1.print("from constructor");
		i1=System.out::println;
		i1.print("On spot");
		
		

	}

}
