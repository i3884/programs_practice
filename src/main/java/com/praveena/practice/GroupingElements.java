package com.praveena.practice;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class GroupingElements {

	public static void main(String[] args) {
		var result=List.of(
				new Movies("A",10,Genre.ACTION),
				new Movies("B",20,Genre.ACTION),
				new Movies("C",20,Genre.THRILLER)
				)
		.stream()
		.collect(Collectors.groupingBy(Movies::getGenre,Collectors.counting()));
		System.out.println(result); 
		
		var result1=List.of(
				new Movies("A",10,Genre.ACTION),
				new Movies("B",20,Genre.ACTION),
				new Movies("C",20,Genre.THRILLER)
				)
		.stream()
		.collect(Collectors.groupingBy(Movies::getGenre,
				                       Collectors.mapping(Movies::getName,
				                       Collectors.joining(","))));
		System.out.println(result1); 
		
		var results=List.of(1,32,1,23,42)
		.stream()
		.collect(Collectors.groupingBy(Integer::valueOf));
		System.out.println(results);
		
		var results4=List.of("sdf","sdfsfd","sdfsdf")
				.stream()
				.collect(Collectors.groupingBy(String::length));
		System.out.println(results4);
	}
}
