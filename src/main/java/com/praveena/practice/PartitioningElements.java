package com.praveena.practice;

import java.util.List;
import java.util.stream.Collectors;

public class PartitioningElements {

	public static void main(String[] args) {

		var result1=List.of(
				new Movies("A",10,Genre.ACTION),
				new Movies("B",20,Genre.ACTION),
				new Movies("C",20,Genre.THRILLER)
				)
		.stream()
		.collect(Collectors.partitioningBy(m->m.getLikes()>10,
				                           Collectors.mapping(Movies::getName,Collectors.joining(" , "))));
		System.out.println(result1);

	}

}
