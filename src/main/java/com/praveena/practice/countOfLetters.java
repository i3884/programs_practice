package com.praveena.practice;

import java.util.stream.Collectors;

public class countOfLetters {

	public static void main(String[] args) {
		String s="Maroon";
  var result=s.chars()
   .mapToObj(c->(char)c)
  .collect(Collectors.groupingBy(Character::new,
		  Collectors.counting()));
		System.out.println(result); 
		
	}

}
