package com.praveena.arrays;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Result1 {

    /*
     * Complete the 'countSwaps' function below.
     *
     * The function accepts INTEGER_ARRAY a as parameter.
     */

    public static int countSwaps(List<Integer> a) {
    	int swaps=0;
     for(int i=0;i<a.size()-1;i++) {
    	 for(int j=0;j<a.size()-i-1;j++) {
    		if(a.get(j)>a.get(j+1)) {
    			int temp=a.get(j);
    			a.set(j,a.get(j+1));
    			a.set(j+1,temp);
    			swaps++;
    		}
    		
    	 }
    	
     }
    	
     return swaps;
    }

}

public class BubbleSort {
    public static void main(String[] args) throws IOException {
        List<Integer> l=Arrays.asList(4,5,3,2,6,1);
        ArrayList<Integer> l1=new ArrayList<>(l);
            System.out.print(Result1.countSwaps(l1));
       System.out.println(l1);
    }
}
