package com.praveena.arrays;

import java.util.List;

public class TwoD {

	public static void main(String[] args) {


		int[][] i=new int[][] {{0,0,0,1},{0,1,1,1},{1,1,1,1}};
		int max=0;
		int maxCol=0;
		for(int row=0;row<i.length;row++) {
			for(int col=0;col<i[row].length;col++) {
				if(i[row][col]==1) {
					if(maxCol<=col) {
						max=row;
					}
					break;
				}
			}
		}

		System.out.println(max);
	}

}
