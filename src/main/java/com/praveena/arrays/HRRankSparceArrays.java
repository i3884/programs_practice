package com.praveena.arrays;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

class Reszdfult {

    /*
     * Complete the 'matchingStrings' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. STRING_ARRAY strings
     *  2. STRING_ARRAY queries
     */

    public static List<Integer> matchingStrings(List<String> strings, List<String> queries) {
 Map<String,Integer> m=new HashMap<String, Integer>();
    	for(String s:strings) {
    		if(m.get(s)==null) {
    			m.put(s, 1);
    		}
    		else {
    			Integer temp=m.get(s);
    			m.put(s, temp+1);
    		}
    		
    	}
    	List<Integer> l=new ArrayList<Integer>(); 
    			
    for(String s:queries) {
    	 if(m.keySet().contains(s)){
    		 l.add(m.get(s));
    	 }
    	 else {
    		 l.add(0);
    	 }
    }
  return l;

    }

}

public class HRRankSparceArrays {
    public static void main(String[] args) throws IOException {
       System.out.println( Reszdfult.matchingStrings(Stream.of("aba","baba","aba","xzxb").collect(toList()), Stream.of("aba","xzxb","ab").collect(toList())));

       
    }
}
