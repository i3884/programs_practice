package com.praveena.arrays;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

class fc {

	/*
	 * Complete the 'arrayManipulation' function below.
	 *
	 * The function is expected to return a LONG_INTEGER. The function accepts
	 * following parameters: 1. INTEGER n 2. 2D_INTEGER_ARRAY queries
	 */

	public static long arrayManipulation(int n, List<List<Integer>> queries) {
		Long max_value = Long.MIN_VALUE;

		List<Integer> l = new ArrayList<>(Collections.nCopies(n+1,0));
		System.out.println(l);
		//System.out.println(l.size());
		
		
		for(List<Integer> l1:queries) {
			int a=l1.get(0);
			int b=l1.get(1);
			int k=l1.get(2);
			int sum=0;
			for(int i=a;i<=b;i++) {
				if(i<l.size()) {
				sum=l.get(i)+k;
				l.set(i, sum);
				if(sum>max_value) {
					max_value=(long) sum;
				}
				
				}
				else {
					l.set(i,k);
					if(k>max_value) {
						max_value=(long) k;
					}
				}
			}
		}
System.out.println(l);
		return max_value;
	}

}

public class HRArrayManipulation {
	public static void main(String[] args) {
		System.out.println(fc.arrayManipulation(5, Stream.of(Stream.of(1, 2, 100).collect(toList()),
				Stream.of(2, 5, 100).collect(toList()), Stream.of(3, 4, 100).collect(toList())).collect(toList())))	;

	}
}
