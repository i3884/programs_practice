package com.praveena.arrays;

import java.util.Arrays;

public class QuickSort {

	public static void main(String[] args) {
		int[] a =new int[] {1, 2, 3};
   quickSort(a,0,2);
   Arrays.asList(a).stream().forEach(System.out::println);
  
	}
	
	public static void quickSort(int[] a ,int left,int right) {
		int index=partition(a,left,right);
		if(left < index) {
			quickSort(a,left,index-1);
		}
		if(index<right) {
			quickSort(a,index,right);
		}
		
	}
	
	static int partition(int a[],int left,int right)
	{
		int i=left;int j=right;
		int pivot=a[(left+right)/2];
		while(i<=j) {
			while(a[i]<pivot) {
				i++;
			}
			while(a[j]>pivot) {
				j--;
			}
			if(i<=j) {
				int temp=a[i];
				a[i]=a[j];
				a[j]=a[i];
				i++;
				j--;
				
			}
		}
		return i;
	}
	

}
