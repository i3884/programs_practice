package com.praveena.interviews.ms;

public class ReverseAStringRecursionAndNormal {

	public static void main(String[] args) {
		String a="P%av&eena";
		String reversed="";
		for(int i=a.length()-1;i>=0;i--){
	       reversed+=a.charAt(i);
		}
		System.out.println(reversed); 
		System.out.println(reverseString(a) );
				
	}
	
	public static String reverseString(String s) {
		if(s==null||s.length()==1)
			return s;
		return reverseString(s.substring(1))+s.charAt(0);
	}
 
}
