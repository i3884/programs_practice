package com.praveena.interviews.ms;

public class ReverseStringWithoutExchangingSpecialCharacters {

	public static void main(String[] args) {
		String s="J0_^sdfs$h";
		char[] str=s.toCharArray();
		int l=0;
		int r=s.length()-1;
		
		while(l<r) {
			if(!isAlphaBet(s.charAt(l)))
				l++;
			else if(!isAlphaBet(s.charAt(r)))
				r--;
			else {
				char tmp = str[l];
                str[l] = str[r];
                str[r] = tmp;
                l++;
                r--;
			}
			}
		
		System.out.println(String.valueOf(str));
	}

	public static boolean isAlphaBet(char ch) {
		return ch>=65 && ch<=90 ||ch>=97&&ch<=122;
	}
}
