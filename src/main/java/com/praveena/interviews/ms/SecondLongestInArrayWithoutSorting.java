package com.praveena.interviews.ms;

public class SecondLongestInArrayWithoutSorting {

	public static void main(String[] args) {
		Integer[] a=new Integer[] {12,32,32,1,22,42};
		int max1=Math.max(a[0], a[a.length-1]);
		int max2=Math.min(a[0], a[a.length-1]);
		for(int i=0;i<a.length;i++) {
			if(a[i]>max1) {
				int temp=max1;
				max1=a[i];
				max2=temp;
			}
			else if(a[i]>=max2 && a[i]!=max1) {
					max2=a[i];
			}
		}
		System.out.println(max1 +" "+max2); 
		

	}

}
