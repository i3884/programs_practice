package com.praveena.interviews.ms;

public class CountTheOccurrencesWithoutUsingCollections {

	public static void main(String[] args) {
	
		String s="Praveena";
		for(int i=0;i<s.length();i++) {
			char ch=s.charAt(i);
			int sum=1;
			for(int j=0;j<s.length();j++) {
				if(i!=j) {
					if(s.charAt(j)==ch) {
						sum++;
					}
				}
			}
			System.out.println("Character "+ch+" count "+sum);
		}

	}

}
