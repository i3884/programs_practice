package com.praveena.interviews.ms;

import java.util.function.Function;
import java.util.stream.Collectors;

public class RemoveTheDuplicatesInTheArray {

	public static void main(String[] args) {
		String s="Praveena";
		s.chars()
		 .mapToObj(c->(char)c)
		 .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
		 .entrySet()
		 .stream()
		 .filter(e->e.getValue()==1)
		 .forEach(e->System.out.println(e.getKey()));
		//interviewer wanted to use collect to set
		System.out.println("0000000000000000000000000000");
		s.chars()
		 .mapToObj(c->(char)c)
		 .collect(Collectors.toSet())
		 .forEach(System.out::println);
		
	}

}
