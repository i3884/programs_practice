package com.praveena.blindcode;

public class ValidMountainArray {

	public static void main(String[] args) {

		Integer[] arr = new Integer[] { 0,1,2,3,4,5,6,7,8,9 };
		System.out.println(isValidMoutainArray(arr));

	}

	public static boolean isValidMoutainArray(Integer[] arr) {

		int i = 0;
		int j = arr.length - 1;
		if (arr.length >= 3) {
			while (i < arr.length && i + 1 < arr.length && arr[i] < arr[i + 1])
				i++;

			while (j > 0 && j - 1 >= 0 && arr[j] < arr[j - 1])
				j--;
			return i>0&&j<arr.length-1&&i==j;
		} else {
			return false;

		}

	}

}
