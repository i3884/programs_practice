package com.praveena.blindcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ContainsDuplicates {

	public static void main(String[] args) {
		System.out.println(containsDuplicate(new int[] {1,1,2,3,4}));
		int[] a={1,1,2,3,4};
		 Set<Integer> s=new HashSet<>();
		
		int count=(int) Arrays.stream(a)
		      .filter(a1->!s.add(a1))
		      .count();
		System.out.println(count);

		System.out.println(count>0);
	}
	
	 public static boolean containsDuplicate(int[] nums) {
		 Set<Integer> s=new HashSet<>();
		 
		 for(int i=0;i<nums.length;i++) {
			 if(!s.add(nums[i])) {
				 return true;
			 }
		 }
	        return false;
	    }

}
