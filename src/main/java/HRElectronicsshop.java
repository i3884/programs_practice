import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;import java.util.function.ToLongBiFunction;
import java.util.regex.*;
import java.util.stream.Stream;

import javax.security.auth.kerberos.KerberosKey;

public class HRElectronicsshop {

    /*
     * Complete the getMoneySpent function below.
     */
    static int getMoneySpent(int[] keyboards, int[] drives, int b) {
    	int max=-1;
      List<Integer> l=new ArrayList<Integer>();
    	for(int i=0;i<keyboards.length;i++) {
    		for(int j=0;j<drives.length;j++) {
    			l.add(keyboards[i]+drives[j]);
    		}
    	}
    for(Integer i:l) {    	
    	if(i<=b&&i>max) {
    		max=i;
    	}
    }
    	
    	return max;

    }


    public static void main(String[] args)  {
    

        int moneySpent = getMoneySpent(new int[] {40,50,60},new int[] {5,8,12},60);
        System.out.println(moneySpent); 
        

    }
}

