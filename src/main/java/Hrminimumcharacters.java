import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class Result3 {

    /*
     * Complete the 'minimumNumber' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER n
     *  2. STRING password
     */

    public static int minimumNumber(int n, String password) {
    	
    	int count=0;
    	Map<Character,Integer> m=new HashMap<>();
    	m.put('s', 0);
    	m.put('c', 0);
    	m.put('l', 0);
    	m.put('d', 0);
    	
 
   
    	for(int i=0;i<password.length();i++) {
    		if(password.charAt(i)>=33  && password.charAt(i)<=47) {
    			m.put('s', m.get('s')+1);
    		}
    		else if(password.charAt(i)>=65  && password.charAt(i)<=90) {
    		
    			m.put('c', m.get('c')+1);
    		}
    		else if(password.charAt(i)>=97  && password.charAt(i)<=122) {
    			
    			m.put('l', m.get('l')+1);
    		}
    		else if(password.charAt(i)>=48  && password.charAt(i)<=57) {
    			
    			m.put('d', m.get('d')+1);
    		}
    		    	    		  	
    }
    for(Integer i:m.values()) {
    	if(i==0) {
    		count++;
    	}
    }
    if((n+count)<6) {
    	count+=6-(n+count);
    }
    
    return count;

    }

}

public class Hrminimumcharacters {
    public static void main(String[] args) throws IOException {
       // BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
       // BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

       // int n = Integer.parseInt(bufferedReader.readLine().trim());

       // String password = bufferedReader.readLine();

       // int answer = Result3.minimumNumber(n, password);
    	int answer = Result3.minimumNumber(3, "abl");
    	System.out.println(answer);

        //bufferedWriter.write(String.valueOf(answer));
        //bufferedWriter.newLine();

        //bufferedReader.close();
        //bufferedWriter.close();
    }
}
