import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Stream;

class zxcv {

	public static int designerPdfViewer(List<Integer> h, String word) {
		
		int max=0;
		for(int i=0;i<word.length();i++) {
			int index=word.charAt(i)-'a';
			int temp=h.get(index);
			if(temp>max) {
				max=temp;	
				}
			
		}

		return max*word.length();

	}

}

public class HRRankDesignerPDFViewer {
	public static void main(String[] args) {

		System.out.println(zxcv.designerPdfViewer(Stream
				.of(1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 7).collect(toList()),
				"zaba"));

	}
}
