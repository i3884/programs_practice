import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

import okhttp3.Challenge;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result4 {

	/*
	 * Complete the 'caesarCipher' function below.
	 *
	 * The function is expected to return a STRING. The function accepts following
	 * parameters: 1. STRING s 2. INTEGER k
	 */

	public static String caesarCipher(String s, int k) {
		StringBuilder b = new StringBuilder(s.length());
		for (int i = 0; i < s.length(); i++) {
			char temp=s.charAt(i);
			Boolean upperCase=Character.isUpperCase(temp);
			
			if(Character.isLetter(temp)) {
				temp+=k;
				if(!Character.isLetter(temp)) {
					temp-=26;
				}
			}
			
				b.append(temp);
			

			
		}
		return b.toString();
	}
}

public class HrCaeserString {
	public static void main(String[] args) throws IOException {
		// BufferedReader bufferedReader = new BufferedReader(new
		// InputStreamReader(System.in));
		// BufferedWriter bufferedWriter = new BufferedWriter(new
		// FileWriter(System.getenv("OUTPUT_PATH")));

		// int n = Integer.parseInt(bufferedReader.readLine().trim());

		// String s = bufferedReader.readLine();

		// int k = Integer.parseInt(bufferedReader.readLine().trim());

		String s = "There's-a-starmaA24 n-waiting-in-the-sky";
		int k = 3;
		String result = Result4.caesarCipher(s, k);
		System.out.println(result);
		// bufferedWriter.write(result);
		// bufferedWriter.newLine();

		// bufferedReader.close();
		// bufferedWriter.close();
	}
}
