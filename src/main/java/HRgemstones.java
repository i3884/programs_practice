import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result10 {

    /*
     * Complete the 'gemstones' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING_ARRAY arr as parameter.
     */

    public static int gemstones(List<String> arr) {
  
    	Map<Character,Integer> m=new HashMap<>();
    	   List<Set<Character>> l=new ArrayList<Set<Character>>();
    	   for(String s:arr) {
    		   s=s.toLowerCase();
    		   char[] chArr=s.toCharArray();
    		   Set<Character> se=new HashSet<>();
    		   for(int i=0;i<chArr.length;i++) {
    			   se.add(s.charAt(i));
    		   }
    		   l.add(se);
    	   }
    	
    	for(Set<Character> s:l) {
    		Iterator<Character> itr=s.iterator();
    		while(itr.hasNext()){
    			char ch=itr.next();
    			if(!m.keySet().isEmpty()) {
    				
    				if(!m.keySet().contains(ch)) {
    					m.put(ch, 1);}
    				else {    				
    					int count=m.get(ch);
    					m.put(ch, count+1);}
    				
    			}
    			else {
    				m.put(ch, 1);
    			}
    			
    		}
    		
    	}
    int count=0;
    for(int i:m.values()) {
    	if(i==arr.size()) {
    		count++;
    	}
    }
    return count;
    }

}

public class HRgemstones {
    public static void main(String[] args) throws IOException {
       /* BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<String> arr = IntStream.range(0, n).mapToObj(i -> {
            try {
                return bufferedReader.readLine();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        })
            .collect(toList());*/

        int result = Result10.gemstones(new ArrayList<String>(Arrays.asList("aabc","abcdd","bcdd")));
      System.out.println(result); 
        

    /*    bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();*/
    }
}
