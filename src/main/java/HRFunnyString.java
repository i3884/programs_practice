import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

class Result9 {

    /*
     * Complete the 'funnyString' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */

    public static String funnyString(String s) {
  s=s.toLowerCase();
  String r1=new StringBuilder(s).reverse().toString();
  
  List<Integer> l1=new ArrayList<>();
  List<Integer> l2=new ArrayList<>();

 for( int i=1;i<s.length();i++) {
l1.add(Math.abs(s.charAt(i)-s.charAt(i-1)));
  }
 for( int i=1;i<r1.length();i++) {
	 l2.add(Math.abs(r1.charAt(i)-r1.charAt(i-1)));
	   }

 if(l1.equals(l2)) {
	 return "Funny";
	  }
 else {
	 return "Not Funny";
 }
      }
    

}

public class HRFunnyString {
    public static void main(String[] args) throws IOException {
       

           System.out.println(Result9.funnyString(""));

    }
}
