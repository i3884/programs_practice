import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class ResultBR {

	/*
	 * Complete the 'breakingRecords' function below.
	 *
	 * The function is expected to return an INTEGER_ARRAY. The function accepts
	 * INTEGER_ARRAY scores as parameter.
	 */

	public static List<Integer> breakingRecords(List<Integer> scores) {
		List<Integer> l = new ArrayList<>();
		int min = 0;
		int max = 0;
		int low = scores.get(0);
		int high = scores.get(0);

		for (int i = 1; i < scores.size(); i++) {
			int value=scores.get(i);
             if(value<low) {
            	 min++;
            	 low=value;
             }
             else if(value >high) {
            	 max++;
            	 high=value;
             }             
		}
		l.add(max);
		l.add(min);
		return l;
	}

}

public class HRBreakingTheRecords {
	public static void main(String[] args) throws IOException {
		System.out.println(ResultBR.breakingRecords(Arrays.asList(3,4,21,36,10,28,35,5,24,42)));

	}
}
