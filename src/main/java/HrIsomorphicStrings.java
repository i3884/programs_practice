import java.util.HashMap;
import java.util.Map;

public class HrIsomorphicStrings {

	public static void main(String[] args) {
		String a="xyz";
		String b="aab";
		Map<Character,Character> m= new HashMap<>();
		System.out.println(m);
		Boolean flag=true;
		for(int i=0;i<a.length();i++) {
			char ca=a.charAt(i);
			char cb=b.charAt(i);
			if(!m.keySet().isEmpty()) {
				if(!m.keySet().contains(ca) ){
					if(!m.values().contains(cb)) {
						flag=false;
					}
					else {
					m.put(ca, cb);}
				}
				else {
					if(!m.get(ca).equals(cb)) {
						flag=false;
						break;
					}
				}
			}
			else {
				m.put(ca, cb);
			}
		}
		if(flag) {
		System.out.println("isomorphic");}
		else {
			System.out.println("non-isomorphic");
		}
	}

}
