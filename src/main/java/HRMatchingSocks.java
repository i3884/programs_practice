import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

class Reszdxflt {

    /*
     * Complete the 'sockMerchant' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER n
     *  2. INTEGER_ARRAY ar
     */

    public static int sockMerchant(int n, List<Integer> ar) {
    	Map<Integer,Integer> m=new HashMap<>();
    	
    	int count =0;
     for(int i=0;i<ar.size();i++) {
    	 Integer i1=ar.get(i);
    	 if(m.get(i1)==null) {
    		 m.put(i1,1);    		 
    	 }
    	 else {
    		 Integer i2=m.get(i1);
    		 System.out.println(i2);
    		 m.put(i1,i2+1);    		 
    	 }
    	      }
     System.out.println(m);
    	
     for(Map.Entry<Integer, Integer> e:m.entrySet()) {
        		 count+=e.getValue()/2;
    	
     }
    	
		return count;
   

    }

}

public class HRMatchingSocks {
    public static void main(String[] args) throws IOException {
       

        int result = Reszdxflt.sockMerchant(9,Stream.of(10,20,20,10,10,30,50,10,20).collect(toList()));
        System.out.println(result);

       
    }
}
