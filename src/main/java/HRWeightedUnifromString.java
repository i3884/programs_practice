import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.IntStream;

class Result8 {

    /*
     * Complete the 'weightedUniformStrings' function below.
     *
     * The function is expected to return a STRING_ARRAY.
     * The function accepts following parameters:
     *  1. STRING s
     *  2. INTEGER_ARRAY queries
     */

    public static List<String> weightedUniformStrings(String s, List<Integer> queries) {
    	s="abbccddde";
    List<String> answer=new ArrayList<>();
    	          int len =  s.length();
                  Set<Integer> set = new HashSet<Integer>();
          int i=0;
          while(i<len){
               int j=i;
               int sum =0;
               while( j<len && s.charAt(i)==s.charAt(j) ){
                   sum += (s.charAt(i)-'a') +1;
                   set.add(sum);
                   j++;
               }
              i = j;
          }
          for(Integer x:queries) {
              if (set.contains(x)){
                 answer.add("Yes");
              }
              else{
            	  answer.add("No");
              }
          }
          return answer;
    }
    

}

public class HRWeightedUnifromString {
    public static void main(String[] args) throws IOException {
             
       // int queriesCount = Integer.parseInt(bufferedReader.readLine().trim());

     System.out.println( Result8.weightedUniformStrings("abccddde", Arrays.asList(1,2,3,4,9,11)));
    }
}
