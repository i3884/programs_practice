package com.praveena.testingproject.register;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Employee {

	private String employee_name;
	private int employee_salary;
	private int age;
	
	@JsonCreator
	public Employee(@JsonProperty("employee_name") String employeeName,@JsonProperty("employee_salary")  int employee_salary,@JsonProperty("age") int age) {
		this.setEmployee_name(employeeName);
		this.employee_salary = employee_salary;
		this.age = age;
	}
	
		public int getEmployee_salary() {
		return employee_salary;
	}
	public void setEmployee_salary(int employee_salary) {
		this.employee_salary = employee_salary;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}


}
