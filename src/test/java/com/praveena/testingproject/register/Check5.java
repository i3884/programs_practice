package com.praveena.testingproject.register;

import java.util.HashSet;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Check5 {

	public static void main(String[] args) {
		String s="praveena";
		s.chars()
		  .mapToObj(c->(char)c)
		  .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
		  .entrySet()
		  .stream()
		  .filter(e->e.getValue()==1L)
		  .map(e->e.getKey())
		  .forEach(System.out::println);
		String s1= "Praveena";
		
		HashSet<Character> h= new HashSet<>();	
		var result=	s1.chars()
			.mapToObj(c->(char)c)
			.filter(c->h.add(c))
			.findFirst()
			.get();
		System.out.println(result);
		

		  String s5="praveena";
		  
		  for(int i=0;i<s5.length();i++) {
			  char ch=s5.charAt(i);
			  int sum =1;
			  for(int j=0;j<s5.length();j++) {
				  if(!(i==j)) {
					   if(s5.charAt(j)==ch) {
						   sum=sum+1;
					   }
				  }
				 
			  }
			  
			  System.out.println("Character "+ch+" count "+sum);
		  }
	}

}
